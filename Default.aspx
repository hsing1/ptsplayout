﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>節目分段暨主控播出資訊表查詢系統</title>
    <link href="EF2K_Forms_Css.css" rel="stylesheet" type="text/css" />
</head>

<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table cellpadding="2" cellspacing="2" border="1">
                <tr>
                    <td>
                        <div>    
                            <asp:Label ID="Label1" runat="server" Text="節目名稱："></asp:Label>
                            <asp:TextBox ID="TextBox_Prog" runat="server" MaxLength="50" Width="300px"></asp:TextBox>
                            <br />
                            <asp:Label ID="Label6" runat="server" Text="節目編號："></asp:Label>
                            <asp:TextBox ID="TextBox_ProgID" runat="server" MaxLength="7"></asp:TextBox>
                            
                            <asp:Button ID="Button_SearchProg" runat="server" Text="查詢節目" />
                            <asp:CheckBox ID="CheckBox_pCheck" runat="server" Text="僅顯示有表單資訊" 
                                AutoPostBack="True" Checked="True" />
                            &nbsp;
                            <asp:Label ID="Label_ErrorMessage" runat="server" Font-Bold="True" 
                                ForeColor="Red"></asp:Label>
                            <br />  
                            <asp:GridView ID="GridView_Prog" runat="server" AllowPaging="True" 
                                AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" 
                                GridLines="None">
                                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                <Columns>                
                                    <asp:TemplateField HeaderText="節目代號" ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton_ProgNo" runat="server" CausesValidation="false" onclick="LinkButton_ProgNo_Click" Text='<%# Eval("FSPROGID") %>'></asp:LinkButton>
                                            <asp:HiddenField ID="HFd_Spec" runat="server" Value ='<%# Eval("Spec") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>                
                                    <asp:BoundField DataField="FSWEBNAME" HeaderText="節目名稱(WEB)" />
                                    <asp:BoundField DataField="FSPGMNAME" HeaderText="節目名稱(內部用)" />
                                    <asp:BoundField DataField="FSPROGOBJNAME" HeaderText="製播目的" />
                                    <asp:BoundField DataField="FSDEPTNAME" HeaderText="製作部門" />
                                    <asp:BoundField DataField="FSPROGSRCNAME" HeaderText="節目來源" />
                                    <asp:BoundField DataField="FNTOTEPISODE" HeaderText="總集數" />   
                                    <asp:BoundField DataField="SpecDesc" HeaderText="規格" />                  
                                    <asp:BoundField DataField="pCheck" HeaderText="表單" />
                                </Columns>
                                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                <EmptyDataTemplate>
                                    查無節目相關資料...
                                </EmptyDataTemplate>
                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                <EditRowStyle BackColor="#999999" />
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                            </asp:GridView>   
                            <br />   
                            <asp:Panel ID="Panel_FormDetail" runat="server" Visible="False" 
                                BorderWidth="1px">
                                    <asp:Label ID="Label4" runat="server" Text="節目代號："></asp:Label>
                                    <asp:Label ID="Label_ProgNo" runat="server" Text=""></asp:Label>
                                    <br />
                                    <asp:Label ID="Label7" runat="server" Text="節目名稱(WEB)："></asp:Label>
                                    <asp:Label ID="Label_FSWEBNAME" runat="server" Text=""></asp:Label>
                                   
                                    <br />
                                    <asp:Label ID="Label5" runat="server" Text="節目名稱(內部用)："></asp:Label>
                                    <asp:Label ID="Label_FSPGMNAME" runat="server" Text=""></asp:Label>
                                    <br />
                                    <br />
                                    <asp:Label ID="Label2" runat="server" Text="子集編號："></asp:Label>        
                                    <asp:DropDownList ID="DropDownList_ProgD" runat="server" AutoPostBack="True">
                                    </asp:DropDownList>
                                    <asp:HiddenField ID="HFd_Spec" runat="server"  />
                                   
                                    <asp:CheckBox ID="CheckBox_pCheck0" runat="server" Text="僅顯示有表單資訊" 
                                        AutoPostBack="True" Checked="True" />
                                   <hr />
                                   <asp:Label ID="Label3" runat="server" Text="節目分段暨主控播出資訊表："></asp:Label>
                                   (編號 - <asp:Label ID="Label_FlowNo" runat="server"></asp:Label> )
                                   <br />
                                   &nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="Label_NoData" runat="server" Text=""></asp:Label>
                                <asp:Panel ID="Panel_PlayOut" runat="server" Visible="false">                         
                                        
                                        <table width="800" border="0" cellspacing="1" cellpadding="0">
                                          <tr>
                                            <td class="bgGery"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr> 
                                                  <td class="header"><div align="center">
                                                      <p class="bgTitleBlue">公視基金會<br /><span class="bgSubTitleWhite">節目分段暨主控播出資訊表</span></p>
                                                    </div></td>
                                                </tr>
                                                <tr> 
                                                  <td><table width="100%" border="0" cellspacing="1" cellpadding="3">
                                                      <tr> 
                                                        <td colspan="4" class="bgTitleLigetBlue"><div align="right"></div></td>
                                                      </tr>              
                                                      <tr> 
                                                        <td class="bgSubTitle">申請人</td>
                                                        <td colspan="3" class="bgRightColumn"> 
                                                            <asp:Label ID="Label_user_Name" runat="server" Text=""></asp:Label>&nbsp;
                                                            <asp:Label ID="Label_Channel" runat="server" Text=""></asp:Label>&nbsp;
                                                            <asp:Label ID="Label_Dept" runat="server" Text=""></asp:Label>&nbsp;
                                                        </td>
                                                      </tr>
                                                      <tr> 
                                                        <td class="bgSubTitle">節目名稱</td>
                                                        <td class="bgRightColumn"> 
                                                            <asp:Label ID="Label_programID" runat="server" Text=""></asp:Label>
                                                            <asp:Label ID="Label_programName" runat="server" Text=""></asp:Label>
                                                        </td>
                                                        <td class="bgSubTitle">分級</td>
                                                        <td class="bgRightColumn"> 
                                                            <asp:Label ID="Label_grade" runat="server" Text=""></asp:Label>
                                                        </td>
                                                      </tr>
                                                      
                                                      <tr> 
                                                        <td height="27" class="bgSubTitle">集別</td>
                                                        <td class="bgRightColumn">
                                                            <asp:Label ID="Label_prgset" runat="server" Text=""></asp:Label>
                                                        </td>
                                                        <td class="bgSubTitle">分集名稱</td>
                                                        <td class="bgRightColumn"> 
                                                            <asp:Label ID="Label_FSPGDNAME" runat="server" Text=""></asp:Label>
                                                          </td>
                                                      </tr>
                                                      <tr>
                                                        <td class="bgSubTitle">製作人</td>
                                                        <td colspan="3" class="bgRightColumn"> 
                                                            <asp:Label ID="Label_programNT" runat="server" Text=""></asp:Label>&nbsp;
                                                            <asp:Label ID="Label_programNTName" runat="server" Text=""></asp:Label>                                 
                                                        </td>
                                                      </tr>
                                                      <tr>
                                                        <td class="bgSubTitle">帶別</td>
                                                        <td class="bgRightColumn">
                                                            <asp:Label ID="Label_categories" runat="server" Text=""></asp:Label>
                                                        </td>
                                                        <td class="bgSubTitle">規格</td>
                                                        <td class="bgRightColumn">
                                                            <asp:Label ID="Label_spec" runat="server" Text=""></asp:Label>
                                                        </td>
                                                      </tr>
                                                      <tr>
                                                        <td class="bgSubTitle">節目總長</td>
                                                        <td class="bgRightColumn" colspan="3">
                                                            <asp:Label ID="Label_length_m" runat="server" Text=""></asp:Label>&nbsp;分&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <asp:Label ID="Label_length_s" runat="server" Text=""></asp:Label>&nbsp;秒
                                                        </td>
                                                      </tr>
                                                      <tr>
                                                        <td class="bgSubTitle">主控插播帶</td>
                                                        <td class="bgRightColumn" colspan="3">
                                                            <table border="0" class="bgRightColumn">
                                                                <tr>
                                                                    <td><b>吸菸卡</b>  </td>
                                                                    <td>
                                                                        <asp:Label ID="Label_smoking" runat="server" Text=""></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td><b>警示卡</b>  </td>
                                                                    <td>
                                                                        <asp:Label ID="Label_alerts" runat="server" Text=""></asp:Label>
                                                                        <asp:Panel ID="Panel_alerts" runat="server" Visible=false>
                                                                            <asp:Label ID="Label_alertsName" runat="server" Text=""></asp:Label>
                                                                            <asp:Panel ID="Panel_custom" runat="server" Visible=false>
                                                                                名稱：<asp:Label ID="Label_customName" runat="server" Text=""></asp:Label>&nbsp;
                                                                                長度：<asp:Label ID="Label_customS" runat="server" Text=""></asp:Label>秒</asp:Panel>                                                    
                                                                        </asp:Panel>
                                                                        
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td valign="top"><b>檔名總片頭</b>  </td>
                                                                    <td valign="top">
                                                                        <asp:Label ID="Label_title" runat="server" Text=""></asp:Label>
                                                                        <asp:Panel ID="Panel_title" runat="server" Visible=false>
                                                                            名稱：<asp:Label ID="Label_titleName" runat="server" Text=""></asp:Label>&nbsp;
                                                                            長度：<asp:Label ID="Label_titles" runat="server" Text=""></asp:Label>秒</asp:Panel>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                      </tr>
                                                      <tr>
                                                        <td class="bgSubTitle">主控鏡面</td>
                                                        <td class="bgRightColumn" colspan="3">
                                                            <table border="0" class="bgRightColumn">
                                                                <tr>
                                                                    <td>節目側標</td>
                                                                    <td>
                                                                         <asp:Label ID="Label_programSide" runat="server" Text=""></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>LIVE側標</td>
                                                                    <td>
                                                                         <asp:Label ID="Label_liveSide" runat="server" Text=""></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>時間側標</td>
                                                                    <td>
                                                                         <asp:Label ID="Label_timeSide" runat="server" Text=""></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>重播</td>
                                                                    <td>
                                                                         <asp:Label ID="Label1_Replay" runat="server" Text=""></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>錄影轉播</td>
                                                                    <td>
                                                                         <asp:Label ID="Label_video" runat="server" Text=""></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>立體聲</td>
                                                                    <td>
                                                                         <asp:Label ID="Label_stereo" runat="server" Text=""></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>頻道LOGO</td>
                                                                    <td>
                                                                         <asp:Label ID="Label_logo" runat="server" Text=""></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td valign="top">雙語</td>
                                                                    <td valign="top">
                                                                        CH1：<asp:Label ID="Label_Ch1" runat="server" Text=""></asp:Label><br />
                                                                        CH2：<asp:Label ID="Label_Ch2" runat="server" Text=""></asp:Label><br />
                                                                        CH3：<asp:Label ID="Label_Ch3" runat="server" Text=""></asp:Label><br />
                                                                        CH4：<asp:Label ID="Label_Ch4" runat="server" Text=""></asp:Label><br />
                                                                    </td>
                                                                </tr>
                                                            </table>                    
                                                        </td>
                                                      </tr>
                                                      <tr>
                                                        <td class="bgSubTitle">備註</td>
                                                        <td class="bgRightColumn" colspan="3">
                                                            <asp:Label ID="Label_Note" runat="server" Text=""></asp:Label>
                                                            <asp:Literal ID="Literal_Note" runat="server"></asp:Literal>
                                                        </td>
                                                      </tr>          
                                                      <tr> 
                                                        <td colspan="4" class="bgTitleLigetBlue"><div align="center"><font color="#000066">[ 破口段數 ]</font></div></td>
                                                      </tr>
                                                      <tr> 
                                                        <td colspan="4" class="bgRightColumn" align="center"> 
                                                            <font color="#000066">
                                                            <asp:GridView ID="GridView_TC" runat="server" AutoGenerateColumns="False" 
                                                                CellPadding="4" ForeColor="#333333" GridLines="None" Width="400px">
                                                                <RowStyle BackColor="#EFF3FB" />
                                                                <Columns>
                                                                    <asp:BoundField DataField="id_auto" HeaderText="破口段數">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="TCBegin" HeaderText="TCBegin">
                                                                        <HeaderStyle HorizontalAlign="Left" />
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="TCEnd" HeaderText="TCEnd">
                                                                        <HeaderStyle HorizontalAlign="Left" />
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="length" HeaderText="實際長度">
                                                                        <HeaderStyle HorizontalAlign="Left" />
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                                                <EmptyDataTemplate>
                                                                    無破口資訊...
                                                                </EmptyDataTemplate>
                                                                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                                <EditRowStyle BackColor="#2461BF" />
                                                                <AlternatingRowStyle BackColor="White" />
                                                            </asp:GridView>
                                                            </font
                                                        </td>
                                                      </tr>  
                                                       
                                                    </table>
                                                    </td>
                                                </tr>
                                              </table>
                                              </td>
                                          </tr>
                                        </table>       
                                
                                </asp:Panel>
                                   
                                                      
                            </asp:Panel>  
                            <br />
                        </div>                       
                    </td>
                </tr>
            </table>
     
        </ContentTemplate>    
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="200">
        <ProgressTemplate>
            <div style="padding: 10px; margin: 10px; position: absolute; top: 200px; left: 200px; background-color: #CCFFCC; width: 500px; height: 30px; vertical-align: middle; text-align: center;">
                <asp:Image ID="Image_ajax_progress_bar" runat="server" ImageUrl="image/ajax-progress-bar.gif" />
                <asp:Label ID="Label_Proc" runat="server" Text="處理中，請稍後..."></asp:Label> 
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>   
    </form>
</body>
</html>
