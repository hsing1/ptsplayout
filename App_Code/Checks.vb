﻿Imports Microsoft.VisualBasic

Public Class Checks
    'Public Const ApplicationType As String = "TEST"
    Public Const ApplicationType As String = "Product"

    Public Function Check_ChineseStr_Length(ByVal str As String, ByVal min As Integer, ByVal max As Integer) As Boolean '判斷字串真實長度(Bit)是否超過設定值
        Dim tempbyte As Byte() = System.Text.Encoding.Default.GetBytes(str)
        If tempbyte.Length >= min And tempbyte.Length <= max Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function Check_StrType(ByVal str As String) As Integer '判斷字串格式 回傳 1.包含中文 2.只有數字 3.其他 4.浮點數
        Dim tempbyte As Byte() = System.Text.Encoding.Default.GetBytes(str)
        If tempbyte.Length <> str.Length Then
            Return 1
        ElseIf Not Regex.IsMatch(str, "\D+") Then
            Return 2
        ElseIf IsNumeric(str) And Regex.IsMatch(str, "\D+") Then
            Return 4
        Else
            Return 3
        End If
    End Function

    Public Function Fun_Get_OrderID(ByVal p_SendType As String, ByVal p_PayType As String, ByVal p_UID As String) As String    '取得訂單編號
        'EXP:   S1A109010764273
        '       1234567890123456
        '1      .寄送代碼 F 郵寄/宅配 S 超商取貨
        '2      .交易代碼 1 信用卡線上刷卡 2ATM轉帳 3貨到付款/取貨付現
        '3      .A 身份證字號第一碼 - 縣市別
        '        A臺北市 B臺中市 C基隆市 D臺南市 E高雄市 F臺北縣 G宜蘭縣 H桃園縣 J新竹縣 K苗栗縣 L臺中縣 M南投縣 N彰化縣 P雲林縣 Q嘉義縣 R臺南縣 S高雄縣 T屏東縣 U花蓮縣 V臺東縣 X澎湖縣 Y陽明山 W金門縣 Z連江縣 O新竹市 I嘉義市 
        '4      .1 身份證字號第二碼 - 性別 1 男 2 女
        '5-10   .日期 090107 = 2009/01/07
        '11-16  .36進位時間序號 64273 （0~43200000 2毫秒)

        '取得日期
        If p_SendType <> "" And p_PayType <> "" Then
            Dim str_DateString As String = Format(Now(), "yyMMdd")
            Dim strTimeString As String = Right("00000" & Fun_10ToHEX(Int(Now().TimeOfDay.TotalMilliseconds) / 2, 36), 5)

            Return p_SendType & p_PayType & p_UID & str_DateString & strTimeString
        Else
            Return ""
        End If
    End Function
    Public Function Fun_10ToHEX(ByVal pValue As Long, ByVal pADD As Integer) As String
        ' 10進位 轉2 ~ 64 進位
        Dim str_OrgaddString As String = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz_@"
        '                                 1234567890123456789012345678901234567890123456789012345678901234
        Dim str_addString As String = Mid(str_OrgaddString, 1, pADD)
        Dim Str_result As String = ""
        Dim Double_CalValue As Double = pValue
        Dim Double_CalChar As Double
        While Double_CalValue <> 0
            Double_CalChar = Double_CalValue Mod pADD
            Str_result = Mid(str_addString, Double_CalChar + 1, 1) & Str_result
            Double_CalValue = (Double_CalValue - Double_CalChar) / pADD
        End While
        Return Str_result
    End Function
    Public Function Fun_HEXto10(ByVal pValue As String, ByVal pADD As Integer) As String
        '2 ~ 64 進位 轉 10進位
        Dim str_OrgaddString As String = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz_@"
        '                                 1234567890123456789012345678901234567890123456789012345678901234
        Dim str_addString As String = Mid(str_OrgaddString, 1, pADD)
        Dim Long_result As Long = 0
        Dim Long_CalVar As Long = 1
        Dim Long_StrLongth As Long = pValue.Length
        Dim int_Count As Long
        For int_Count = Long_StrLongth To 1 Step -1
            Long_result += Long_CalVar * (str_addString.IndexOf(Mid(pValue, int_Count, 1)))
            Long_CalVar *= pADD
        Next
        Return Long_result
    End Function

    Public Function FormatStr(ByVal strText As String) As String
        Dim Htmlstring As String = strText
        '删除脚本   
        Htmlstring = Regex.Replace(Htmlstring, "<script[^>]*?>.*?</script>", "", RegexOptions.IgnoreCase)
        '删除HTML   
        Htmlstring = Regex.Replace(Htmlstring, "<(.[^>]*)>", "", RegexOptions.IgnoreCase)
        Htmlstring = Regex.Replace(Htmlstring, "([\r\n])[\s]+", "", RegexOptions.IgnoreCase)
        Htmlstring = Regex.Replace(Htmlstring, "-->", "", RegexOptions.IgnoreCase)
        Htmlstring = Regex.Replace(Htmlstring, "<!--.*", "", RegexOptions.IgnoreCase)
        Htmlstring = Regex.Replace(Htmlstring, "(quot|#34);", "\'", RegexOptions.IgnoreCase)
        Htmlstring = Regex.Replace(Htmlstring, "(amp|#38);", "", RegexOptions.IgnoreCase)
        Htmlstring = Regex.Replace(Htmlstring, "(lt|#60);", "<", RegexOptions.IgnoreCase)
        Htmlstring = Regex.Replace(Htmlstring, "(gt|#62);", ">", RegexOptions.IgnoreCase)
        Htmlstring = Regex.Replace(Htmlstring, "(nbsp|#160);", "   ", RegexOptions.IgnoreCase)
        Htmlstring = Regex.Replace(Htmlstring, "(iexcl|#161);", "\xa1", RegexOptions.IgnoreCase)
        Htmlstring = Regex.Replace(Htmlstring, "(cent|#162);", "\xa2", RegexOptions.IgnoreCase)
        Htmlstring = Regex.Replace(Htmlstring, "(pound|#163);", "\xa3", RegexOptions.IgnoreCase)
        Htmlstring = Regex.Replace(Htmlstring, "(copy|#169);", "\xa9", RegexOptions.IgnoreCase)
        Htmlstring = Regex.Replace(Htmlstring, "#(\d+);", "", RegexOptions.IgnoreCase)
        Htmlstring.Replace("<", "")
        Htmlstring.Replace(">", "")
        Htmlstring.Replace("\r\n", "")
        Htmlstring = HttpContext.Current.Server.HtmlEncode(Htmlstring).Trim()
        Return Htmlstring
    End Function

    Public Function ValidatePassword(ByVal password As String, ByVal minLength As Integer, ByVal maxLength As Integer, ByVal numLetters As Integer, ByVal numNumbers As Integer, ByVal numSpecial As Integer) As Boolean
        '建立檢核邏輯        
        Dim letter As New Regex("[a-zA-Z]")
        Dim number As New Regex("[0-9]")
        '不屬於大、小寫英文及數字的邏輯        
        Dim special As New Regex("[^a-zA-Z0-9]")
        '檢核密碼        
        '長度必須大於等於設定值
        If password.Trim().Length < minLength Then Return False
        If maxLength > 0 Then
            If password.Trim().Length > maxLength Then Return False
        End If ' maxLength
        '大小寫英文必須大於設定值
        If (letter.Matches(password).Count < numLetters) Then Return False
        '數字必須大於設定值
        If (number.Matches(password).Count < numNumbers) Then Return False

        If numSpecial > 0 Then '假如設定值大於代表有特殊字元
            '不屬於大、小寫英文及數字的其它符號必須大於設定值
            If (special.Matches(password).Count < numSpecial) Then Return False
        Else
            If (special.Matches(password).Count > numSpecial) Then Return False
        End If

        Return True
    End Function

    Public Function Fun_GetSaleType(ByVal pObj As Object) As String
        '抓取身份別
        Dim str_SaleType As String
        Try
            str_SaleType = FormsAuthentication.Decrypt(pObj.Request.Cookies(FormsAuthentication.FormsCookieName).Value).UserData '取得身份
        Catch ex As Exception
            str_SaleType = ""
        End Try
        Return str_SaleType
    End Function
    Public Function Fun_GetID(ByVal pObj As Object) As String
        '抓取id
        Dim str_ID As String
        Try
            str_ID = FormsAuthentication.Decrypt(pObj.Request.Cookies(FormsAuthentication.FormsCookieName).Value).Name '取得ID
        Catch ex As Exception
            str_ID = ""
        End Try
        Return str_ID
    End Function
    Public Function Fun_ChechAdminIP(ByVal pIP As String) As Boolean
        If pIP = "127.0.0.1" Then
            Fun_ChechAdminIP = True
        ElseIf Mid(pIP, 1, 5) = "10.1." Then
            Fun_ChechAdminIP = True
        Else
            Fun_ChechAdminIP = False
        End If
    End Function
    '檢核身份證字號正確性
    Public Function Fun_checkID(ByVal sPID As String) As Boolean
        Try
            Dim iChkNum As Integer
            If sPID.Length = 10 Then
                ' 第 1 碼
                sPID = UCase(sPID)
                Dim std As String = "ABCDEFGHJKLMNPQRSTUVXYWZIO"
                iChkNum = Microsoft.VisualBasic.Trim(Str(std.IndexOf(Mid(sPID, 1, 1)) + 10))
                iChkNum = Int(iChkNum / 10) + (iChkNum Mod 10) * 9
                '第 2 - 9 碼
                Dim i As Integer
                For i = 2 To sPID.Length - 1
                    iChkNum += Mid(sPID, i, 1) * (10 - i)
                Next
                iChkNum = 10 - (iChkNum Mod 10)
                If iChkNum > 9 Then
                    iChkNum = 10 - iChkNum
                End If

                '檢查碼
                Return iChkNum = Val(Mid(sPID, 10, 1))
            Else
                Return False
            End If
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
End Class
