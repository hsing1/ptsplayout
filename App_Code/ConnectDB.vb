﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Reflection
Imports System.Security.Cryptography
'匯入OleDb命名空間
Imports System.Data.OleDb
Imports System.IO
Imports System.Web.Hosting
Imports System.Text.RegularExpressions
Imports System.ComponentModel
Imports Checks
Imports System.Text
Public Enum ChkType
    Replace = 1
    Blank = 2
    Clean = 3
    CleanAll = 4
    NoAction = 5
End Enum
Public Enum ServerPath
    <Description("pts-intranet")> DBptsintranet = 1              '活動主機DB

End Enum
Public Enum ServerDB
    <Description("PTSProgram")> PTSPlayOut = 1                'PLAYOUT 主控播出資訊表查詢
End Enum

Public Class ConnectDB
    '查詢連線參數定義的字串
    Private Shared Function GetCustomEnumValue(ByVal inputEnum As Object) _
    As String
        Dim currentEnumObj As Reflection.FieldInfo = inputEnum.GetType().GetField(inputEnum.ToString())
        Dim attributesForEnum As Object() = currentEnumObj.GetCustomAttributes(False)
        If attributesForEnum.Length = 0 Then
            Return String.Empty
        Else
            Dim attributeForDesc As Object = CType(attributesForEnum(0), System.ComponentModel.DescriptionAttribute)
            Return attributeForDesc.Description
        End If
    End Function
    '取得DB型別
    Public Function Fun_Get_DBType() As String
        Fun_Get_DBType = "System.Data.OleDb"
    End Function
    '取得DataSet
    Public Function Fun_Get_DataSet(ByVal p_Str_SQL As String, _
                                ByVal p_Str_SysId As ServerDB, _
                                ByVal p_Str_Dsn As ServerPath) As DataSet
        Dim wk_Str_Connectstring As String
        Dim wk_Data_DataAdapter As New Data.OleDb.OleDbDataAdapter       '資料庫溝通物件
        Dim wk_ds_Fun_Get_DataSet As New DataSet
        p_Str_SQL = Trim(p_Str_SQL)
        Try
            wk_Str_Connectstring = Fun_Get_Connectstring(p_Str_SysId, p_Str_Dsn)
            If wk_Str_Connectstring = "" Then
                Fun_Get_DataSet = Nothing
            Else
                '限制僅能Select
                Dim wk_Key_Word() As String = Split(p_Str_SQL, " ")
                If UCase(Trim(wk_Key_Word(0))) <> "SELECT" Then
                    p_Str_SQL = "警告 " & p_Str_SQL
                    Fun_Get_DataSet = Nothing
                    Throw New System.Exception("指令不合法")
                Else
                    '使用Using 區塊確保資料庫連線釋放
                    Using wk_Data_Connection As Data.OleDb.OleDbConnection = New Data.OleDb.OleDbConnection(wk_Str_Connectstring)
                        wk_Data_Connection.Open()
                        wk_Data_DataAdapter.SelectCommand = New Data.OleDb.OleDbCommand(p_Str_SQL, wk_Data_Connection)
                        wk_Data_DataAdapter.Fill(wk_ds_Fun_Get_DataSet)
                        Fun_Get_DataSet = wk_ds_Fun_Get_DataSet
                        wk_Data_Connection.Close()
                    End Using
                End If
            End If
        Catch ex As Exception
            Fun_Get_DataSet = Nothing
            Throw New System.Exception("*E* Get DataSet Error .. (" & ex.Message & "  SQL:" & p_Str_SQL & ")")
        End Try
        wk_Str_Connectstring = Nothing
        wk_Data_DataAdapter = Nothing
        wk_ds_Fun_Get_DataSet = Nothing
    End Function
    '取得DataTable
    Public Function Fun_Get_DataTable(ByVal p_Str_SQL As String, _
                                    ByVal p_Str_SysId As ServerDB, _
                                    ByVal p_Str_Dsn As ServerPath) As DataTable
        Dim wk_Str_Connectstring As String
        Dim wk_Data_DataAdapter As New Data.OleDb.OleDbDataAdapter       '資料庫溝通物件
        Dim wk_dt_Fun_Get_DataTable As New DataTable
        p_Str_SQL = Trim(p_Str_SQL)
        Try
            wk_Str_Connectstring = Fun_Get_Connectstring(p_Str_SysId, p_Str_Dsn)
            If wk_Str_Connectstring = "" Then
                Fun_Get_DataTable = Nothing
            Else
                '限制僅能Select
                Dim wk_Key_Word() As String = Split(p_Str_SQL, " ")
                If UCase(Trim(wk_Key_Word(0))) <> "SELECT" Then
                    p_Str_SQL = "警告 " & p_Str_SQL
                    Fun_Get_DataTable = Nothing
                    Throw New System.Exception("指令不合法")
                End If
                '使用Using 區塊確保資料庫連線釋放
                Using wk_Data_Connection As Data.OleDb.OleDbConnection = New Data.OleDb.OleDbConnection(wk_Str_Connectstring)
                    wk_Data_Connection.Open()
                    wk_Data_DataAdapter.SelectCommand = New Data.OleDb.OleDbCommand(p_Str_SQL, wk_Data_Connection)
                    wk_Data_DataAdapter.Fill(wk_dt_Fun_Get_DataTable)
                    Fun_Get_DataTable = wk_dt_Fun_Get_DataTable
                    wk_Data_Connection.Close()
                End Using
            End If
        Catch ex As Exception
            Fun_Get_DataTable = Nothing
            Throw New System.Exception("*E* Get DataTable Error .. (" & ex.Message & "  SQL:" & p_Str_SQL & ")")
        End Try
        wk_Str_Connectstring = Nothing
        wk_Data_DataAdapter = Nothing
        wk_dt_Fun_Get_DataTable = Nothing
    End Function
    '取得Value
    Public Function Fun_Get_Value(ByVal p_Str_SQL As String, _
                                    ByVal p_Str_SysId As ServerDB, _
                                    ByVal p_Str_Dsn As ServerPath) As String
        Dim wk_Str_Connectstring As String
        Dim wk_Data_DataAdapter As New Data.OleDb.OleDbDataAdapter       '資料庫溝通物件
        Dim wk_dt_Fun_Get_DataTable As New DataTable
        p_Str_SQL = Trim(p_Str_SQL)
        Try
            wk_Str_Connectstring = Fun_Get_Connectstring(p_Str_SysId, p_Str_Dsn)
            If wk_Str_Connectstring = "" Then
                Fun_Get_Value = ""
            Else
                '限制僅能Select
                Dim wk_Key_Word() As String = Split(p_Str_SQL, " ")
                If UCase(Trim(wk_Key_Word(0))) <> "SELECT" Then
                    p_Str_SQL = "警告 " & p_Str_SQL
                    Fun_Get_Value = Nothing
                    Throw New System.Exception("指令不合法")
                End If
                '使用Using 區塊確保資料庫連線釋放
                Using wk_Data_Connection As Data.OleDb.OleDbConnection = New Data.OleDb.OleDbConnection(wk_Str_Connectstring)
                    wk_Data_Connection.Open()
                    wk_Data_DataAdapter.SelectCommand = New Data.OleDb.OleDbCommand(p_Str_SQL, wk_Data_Connection)
                    wk_Data_DataAdapter.Fill(wk_dt_Fun_Get_DataTable)
                    '僅支援抓取單筆資料
                    If wk_dt_Fun_Get_DataTable.Rows.Count() = 1 Then
                        Fun_Get_Value = wk_dt_Fun_Get_DataTable.Rows(0).Item(0).ToString
                    Else
                        Fun_Get_Value = ""
                    End If
                    wk_Data_Connection.Close()
                End Using
            End If
        Catch ex As Exception
            Fun_Get_Value = ""
            Throw New System.Exception("*E* Get Value Error .. (" & ex.Message & "  SQL:" & p_Str_SQL & ")")
        End Try
        wk_Str_Connectstring = Nothing
        wk_Data_DataAdapter = Nothing
        wk_dt_Fun_Get_DataTable = Nothing
    End Function
    '執行SQL Command
    Public Function Fun_Execute_Command(ByVal p_Str_SQL As String, _
                                    ByVal p_Str_SysId As ServerDB, _
                                    ByVal p_Str_Dsn As ServerPath) As Boolean
        Dim wk_Str_Connectstring As String
        Dim wk_Data_Command As New Data.OleDb.OleDbCommand               'SQL指令物件
        Dim wk_Data_Transaction As Data.OleDb.OleDbTransaction           '交易物件
        Dim wk_Bool_ChkTransaction As Boolean = False                    'Transaction狀態Check值
        Dim wk_Int_ArrayExecute As Integer                               '交易迴圈
        '取消最後的分號
        p_Str_SQL = Trim(p_Str_SQL)
        If Right(p_Str_SQL, 1) = ";" Then
            p_Str_SQL = Left(p_Str_SQL, p_Str_SQL.Length - 1)
        End If
        Try
            Dim wk_Str_SqlArray() As String = Split(p_Str_SQL, ";")
            Dim wk_Int_ArrayCount As Integer = UBound(wk_Str_SqlArray)
            wk_Str_Connectstring = Fun_Get_Connectstring(p_Str_SysId, p_Str_Dsn)
            If wk_Str_Connectstring = "" Then
                Fun_Execute_Command = Nothing
            Else
                If wk_Int_ArrayCount = 0 Then
                    Using wk_Data_Connection As Data.OleDb.OleDbConnection = New Data.OleDb.OleDbConnection(wk_Str_Connectstring)
                        wk_Data_Connection.Open()
                        '初始化Command物件
                        wk_Data_Command.Connection = wk_Data_Connection
                        '執行SQL 判斷交易情形
                        Try
                            '限制僅能 編修資料
                            Dim wk_Key_Word() As String = Split(Trim(wk_Str_SqlArray(0)), " ")
                            If Not (UCase(Trim(wk_Key_Word(0))) = "INSERT" Or UCase(Trim(wk_Key_Word(0))) = "DELETE" Or UCase(Trim(wk_Key_Word(0))) = "UPDATE") Then
                                wk_Str_SqlArray(0) = "警告 " & wk_Str_SqlArray(0)
                                Throw New System.Exception("指令不合法")
                            Else
                                wk_Data_Command.CommandText = wk_Str_SqlArray(0)
                                wk_Data_Command.ExecuteNonQuery()
                                '交易結束
                                wk_Data_Connection.Close()
                            End If
                        Catch ex As Exception
                            Throw New System.Exception(ex.Message & "  SQL:" & wk_Str_SqlArray(0))
                        End Try
                        Fun_Execute_Command = True
                    End Using
                Else '執行交易
                    '使用Using 區塊確保資料庫連線釋放
                    Using wk_Data_Connection As Data.OleDb.OleDbConnection = New Data.OleDb.OleDbConnection(wk_Str_Connectstring)
                        wk_Data_Connection.Open()
                        '交易開始
                        wk_Data_Transaction = wk_Data_Connection.BeginTransaction(IsolationLevel.ReadCommitted)
                        wk_Bool_ChkTransaction = True
                        '初始化Command物件
                        wk_Data_Command.Transaction = wk_Data_Transaction
                        wk_Data_Command.Connection = wk_Data_Connection
                        '執行SQL 判斷交易情形
                        Try
                            For wk_Int_ArrayExecute = 0 To wk_Int_ArrayCount
                                If wk_Str_SqlArray(wk_Int_ArrayExecute) <> "" Then
                                    '限制僅能 編修資料
                                    Dim wk_Key_Word() As String = Split(Trim(wk_Str_SqlArray(wk_Int_ArrayExecute)), " ")
                                    If Not (UCase(Trim(wk_Key_Word(0))) = "INSERT" Or UCase(Trim(wk_Key_Word(0))) = "DELETE" Or UCase(Trim(wk_Key_Word(0))) = "UPDATE") Then
                                        wk_Str_SqlArray(wk_Int_ArrayExecute) = "警告 " & wk_Str_SqlArray(wk_Int_ArrayExecute)
                                        Throw New System.Exception("指令不合法")
                                    Else
                                        wk_Data_Command.CommandText = wk_Str_SqlArray(wk_Int_ArrayExecute)
                                        wk_Data_Command.ExecuteNonQuery()
                                    End If
                                End If
                            Next
                            '交易結束
                            wk_Data_Transaction.Commit()
                            wk_Data_Connection.Close()
                        Catch ex As Exception
                            If wk_Bool_ChkTransaction Then
                                wk_Data_Transaction.Rollback()
                            End If
                            Throw New System.Exception(ex.Message & "  SQL:" & wk_Str_SqlArray(wk_Int_ArrayExecute))
                        End Try
                        Fun_Execute_Command = True
                    End Using
                End If
            End If
        Catch ex As Exception
            Fun_Execute_Command = False
            Throw New System.Exception("*E* Execute SQL Error .. (" & ex.Message & ")")
        End Try
        wk_Str_Connectstring = Nothing
        wk_Data_Command = Nothing
    End Function
    '傳回Connection String    
    Public Function Fun_Get_Connectstring(ByVal p_Str_SysId As ServerDB, ByVal p_Str_Dsn As ServerPath, Optional ByVal pDefaultMode As Boolean = True) As String
        Try
            Fun_Get_Connectstring = Fun_Inq_Connectstring(GetCustomEnumValue(p_Str_SysId), GetCustomEnumValue(p_Str_Dsn), pDefaultMode)
        Catch
            Fun_Get_Connectstring = ""
        End Try
    End Function
    '查詢連線字串 
    Private Function Fun_Inq_Connectstring(ByVal p_Str_SysId As String, ByVal p_Str_Dsn As String, ByVal pDefaultMode As Boolean) As String
        '開啟資料庫連線()
        'SQL驗證()
        '抓取設定檔()
        Dim fr As StreamReader
        Dim ReadString As String
        ReadString = ""
        Dim FileString As String
        '抓取文字檔
        'fr = New StreamReader(HostingEnvironment.ApplicationPhysicalPath() & "\App_Code" & "\ConnectDB")
        fr = New StreamReader(System.Web.Hosting.HostingEnvironment.MapPath("~") & "\ConnectDB")

        Do
            FileString = fr.ReadLine
            ReadString = ReadString & FileString & vbCrLf
        Loop Until (FileString = Nothing)
        fr.Close()
        ReadString = DeEncode(ReadString)
        Dim wk_Str_iniArray() As String = Split(ReadString, vbCrLf)
        Dim wk_Int_iniCount As Integer = UBound(wk_Str_iniArray)
        Dim strID As String = ""
        Dim strPWD As String = ""
        Dim wk_Int_ArrayCount As Integer
        For wk_Int_ArrayCount = 0 To wk_Int_iniCount
            If UCase(p_Str_SysId) = UCase(wk_Str_iniArray(wk_Int_ArrayCount)) Then
                strID = wk_Str_iniArray(wk_Int_ArrayCount + 1)
                strPWD = wk_Str_iniArray(wk_Int_ArrayCount + 2)
            End If
        Next
        If pDefaultMode Then
            Fun_Inq_Connectstring = "Provider=SQLOLEDB;Data Source=" & p_Str_Dsn & ";Initial Catalog=" & p_Str_SysId & ";Persist Security Info=True;User ID=" & strID & ";Password=" & strPWD
        Else
            Fun_Inq_Connectstring = "Data Source=" & p_Str_Dsn & ";Initial Catalog=" & p_Str_SysId & ";Persist Security Info=True;User ID=" & strID & ";Password=" & strPWD
        End If

    End Function
    '加解密
    Private Function EnEncode(ByVal FormCode As String) As String
        Dim EncodeView As New StringBuilder
        Dim i As Integer
        Dim jj As Integer
        jj = Len(Trim(FormCode))
        For i = 1 To jj Step 1
            EncodeView.Append(StringEnDeCodecn(Mid(Trim(FormCode), i, 1), i Mod 5) & Chr(CInt(Int((60 * Rnd()) + 40))))
        Next i
        Return EncodeView.ToString
    End Function
    Private Function DeEncode(ByVal FormCode As String) As String
        Dim EncodeView As New StringBuilder
        Dim i As Integer
        Dim jj As Integer
        jj = Len(Trim(FormCode))
        For i = 1 To jj Step 2
            EncodeView.Append(StringEnDeCodecn(Mid(Trim(FormCode), i, 1), -((i + 1) / 2 Mod 5)))
        Next i
        Return Replace(EncodeView.ToString, Chr(20), "")
    End Function
    Private Function StringEnDeCodecn(ByVal strSource As String, ByVal rndBit As String) As String
        Dim CHARNUM As Long
        Dim SINGLECHAR As String
        Dim strTmp As String = ""
        Dim i As Integer
        For i = 1 To Len(strSource) Step 1
            SINGLECHAR = Mid(strSource, i, 1)
            CHARNUM = Asc(SINGLECHAR)
            If CHARNUM = 13 Or CHARNUM = 10 Then
                strTmp = strTmp & Chr(20)
            ElseIf CHARNUM = 20 Then
                strTmp = strTmp & vbCrLf
            Else
                CHARNUM = CHARNUM + rndBit
                strTmp = strTmp & Chr(CHARNUM)
            End If
        Next i
        Return strTmp
        Exit Function
    End Function
    '欄位驗證    
    Public Function Fun_Chk_DataField(ByVal p_String As String, Optional ByVal p_Type As ChkType = ChkType.Replace) As String
        'Type 定義
        'Replace = 1    取代為其他字串
        'Blank = 2      取代為空白
        'Clean = 3      關鍵字串清除
        'CleanAll=4     全字串清除
        'NoAction = 5   無動作
        Const Const_CharCount As Integer = 7
        Dim int_Count As Integer
        '字元比對字串
        Dim str_ChkArray(2, Const_CharCount) As String
        '尋找字元
        str_ChkArray(1, 1) = "%27"
        str_ChkArray(1, 2) = "'"
        str_ChkArray(1, 3) = "%23"
        str_ChkArray(1, 4) = "#"
        str_ChkArray(1, 5) = "%2D%2D"
        str_ChkArray(1, 6) = "--"
        str_ChkArray(1, 7) = ";"
        '取代字元
        str_ChkArray(2, 1) = "`"
        str_ChkArray(2, 2) = "`"
        str_ChkArray(2, 3) = "_"
        str_ChkArray(2, 4) = "_"
        str_ChkArray(2, 5) = "__"
        str_ChkArray(2, 6) = "__"
        str_ChkArray(2, 7) = ":"
        If p_Type <> ChkType.NoAction And p_String <> "" Then
            For int_Count = 1 To Const_CharCount
                If p_String <> "" Then
                    Select Case p_Type
                        Case ChkType.Replace
                            p_String = Replace(p_String, str_ChkArray(1, int_Count), str_ChkArray(2, int_Count))
                        Case ChkType.Blank
                            p_String = Replace(p_String, str_ChkArray(1, int_Count), " ")
                        Case ChkType.Clean
                            p_String = Replace(p_String, str_ChkArray(1, int_Count), "")
                        Case ChkType.CleanAll
                            If p_String.IndexOf(str_ChkArray(1, int_Count)) > 0 Then
                                p_String = ""
                            End If
                        Case Else
                            p_String = p_String
                    End Select
                End If
            Next
        End If
        Fun_Chk_DataField = p_String
    End Function
    '執行單筆SQL Command ，請注意 SQL Injection 攻擊可能性
    Public Function Fun_Execute_OneCommand(ByVal p_Str_SQL As String, _
                                    ByVal p_Str_SysId As ServerDB, _
                                    ByVal p_Str_Dsn As ServerPath) As Boolean
        Dim wk_Str_Connectstring As String
        Dim wk_Data_Command As New Data.OleDb.OleDbCommand               'SQL指令物件
        Dim wk_Bool_ChkTransaction As Boolean = False                    'Transaction狀態Check值
        '取消最後的分號
        p_Str_SQL = Trim(p_Str_SQL)
        Try
            wk_Str_Connectstring = Fun_Get_Connectstring(p_Str_SysId, p_Str_Dsn)
            If wk_Str_Connectstring = "" Then
                Fun_Execute_OneCommand = Nothing
            Else
                Using wk_Data_Connection As Data.OleDb.OleDbConnection = New Data.OleDb.OleDbConnection(wk_Str_Connectstring)
                    wk_Data_Connection.Open()
                    '初始化Command物件
                    wk_Data_Command.Connection = wk_Data_Connection
                    '執行SQL 判斷交易情形
                    Try
                        '限制僅能 編修資料
                        Dim wk_Key_Word() As String = Split(Trim(p_Str_SQL), " ")
                        If Not (UCase(Trim(wk_Key_Word(0))) = "INSERT" Or UCase(Trim(wk_Key_Word(0))) = "DELETE" Or UCase(Trim(wk_Key_Word(0))) = "UPDATE") Then
                            Throw New System.Exception("指令不合法")
                        Else
                            wk_Data_Command.CommandText = p_Str_SQL
                            wk_Data_Command.ExecuteNonQuery()
                            '交易結束
                            wk_Data_Connection.Close()
                        End If
                    Catch ex As Exception
                        Throw New System.Exception(ex.Message & "  SQL:" & p_Str_SQL)
                    End Try
                    Fun_Execute_OneCommand = True
                End Using
            End If
        Catch ex As Exception
            Fun_Execute_OneCommand = False
            Throw New System.Exception("*E* Execute SQL Error .. (" & ex.Message & ")")
        End Try
        wk_Str_Connectstring = Nothing
        wk_Data_Command = Nothing
    End Function
End Class
