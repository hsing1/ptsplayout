﻿
Partial Class _Default
    Inherits System.Web.UI.Page

    Protected Sub Button_SearchProg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button_SearchProg.Click
        Call sub_SearchProg()
    End Sub

    Private Sub sub_SearchProg()
        '查詢節目
        Try
            Dim objConnectDB As New ConnectDB
            Dim strSQL As New StringBuilder
            strSQL.Append("SELECT Distinct a.[FSPROGID],a.[FSWEBNAME],a.[FSPGMNAME],a.[FSPROGOBJNAME],a.[FSDEPTNAME],a.[FSPROGSRCNAME],a.[FNTOTEPISODE], CASE when isnull(b.[playout001],'*') ='*' then '' else 'Y' end as pCheck ")
            strSQL.Append(", (Case when  b.spec = 1 then 'SD' else 'HD' end) as SpecDesc , b.spec")
            strSQL.Append(" FROM [PTSProgram].[dbo].[vwTBSProgInfo] as a ")
            '檢查對應表單
            If CheckBox_pCheck.Checked Then
                strSQL.Append(" inner join ")
            Else
                strSQL.Append(" left outer join  ")

            End If
            '2011/04/22 只抓取審核通過表單
            strSQL.Append(" (")
            strSQL.Append(" 	select b.* from [10.1.253.88].[EF2KWeb].[EZAdmin].[playout] as b")
            strSQL.Append(" 	inner join [10.1.253.88].[EF2KWeb].[dbo].[resda] as c on b.PLAYOUT001=c.resda001 and b.PLAYOUT002=c.resda002  and c.resda021=2")
            strSQL.Append(" ) as b on a.[FSPROGID] = b.[programID] COLLATE Chinese_Taiwan_Stroke_CI_AS ")
            strSQL.Append(" where 1=1 ")
            If TextBox_Prog.Text <> "" Then
                strSQL.Append(" and a.[FSWEBNAME] like '%" & objConnectDB.Fun_Chk_DataField(TextBox_Prog.Text) & "%' or a.[FSPGMNAME] like '%" & objConnectDB.Fun_Chk_DataField(TextBox_Prog.Text) & "%' ")
            End If
            If TextBox_ProgID.Text <> "" Then
                strSQL.Append(" and a.[FSPROGID] = '" & objConnectDB.Fun_Chk_DataField(TextBox_ProgID.Text) & "';")
            End If
            ViewState("SearchProg_SQL") = strSQL.ToString
            GridView_Prog.DataSource = objConnectDB.Fun_Get_DataTable(strSQL.ToString, ServerDB.PTSPlayOut, ServerPath.DBptsintranet)
            GridView_Prog.DataBind()
            If GridView_Prog.Rows.Count = 0 Then
                Panel_FormDetail.Visible = False
            End If
        Catch ex As Exception
            Label_ErrorMessage.Text = "查詢節目 發生錯誤"
        End Try

    End Sub

    Protected Sub GridView_Prog_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView_Prog.PageIndexChanging
        '查詢節目 分頁
        Try
            Dim objConnectDB As New ConnectDB
            GridView_Prog.DataSource = objConnectDB.Fun_Get_DataTable(ViewState("SearchProg_SQL").ToString, ServerDB.PTSPlayOut, ServerPath.DBptsintranet)
            GridView_Prog.PageIndex = e.NewPageIndex
            GridView_Prog.DataBind()
        Catch ex As Exception
            Label_ErrorMessage.Text = "節目分頁 發生錯誤"
        End Try

    End Sub

    Protected Sub LinkButton_ProgNo_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objConnectDB As New ConnectDB
        Dim strSQL As New StringBuilder
        Dim str_RowID As String = CType(sender, LinkButton).CommandArgument
        Dim str_FSPROGID As String = CType(GridView_Prog.Rows(str_RowID).Cells(0).FindControl("LinkButton_ProgNo"), LinkButton).Text
        Dim str_SPec As String = CType(GridView_Prog.Rows(str_RowID).Cells(0).FindControl("HFd_Spec"), HiddenField).Value
        Dim str_FSWEBNAME As String = GridView_Prog.Rows(str_RowID).Cells(1).Text
        Dim str_FSPGMNAME As String = GridView_Prog.Rows(str_RowID).Cells(2).Text
        '顯示選取資訊
        Panel_FormDetail.Visible = True
        Label_ProgNo.Text = str_FSPROGID
        HFd_Spec.Value = str_SPec
        Label_FSWEBNAME.Text = str_FSWEBNAME
        Label_FSPGMNAME.Text = str_FSPGMNAME
        '顯示From資訊
        Call sub_ShowFormDetail(str_FSPROGID, str_SPec)
    End Sub


    Private Sub sub_ShowFormDetail(ByVal p_FSPROGID As String, ByVal p_Spec As String)
        Dim objConnectDB As New ConnectDB
        Dim strSQL As New StringBuilder
        '產生子集下拉清單
        Try
            strSQL.Append("SELECT Distinct a.[FNEPISODE],CAST(a.[FNEPISODE] as varchar) + ' - ' + a.[FSPGDNAME] + CASE when isnull(b.[playout001],'*') ='*' then '' else ' ***' end as pTitle  FROM [PTSProgram].[dbo].[TBPROG_D]  as a ")
            If CheckBox_pCheck0.Checked Then
                strSQL.Append(" inner join ")
            Else
                strSQL.Append(" left outer join ")
            End If

            '2011/04/22 只抓取審核通過表單
            strSQL.Append(" (")
            strSQL.Append(" 	select b.* from [10.1.253.88].[EF2KWeb].[EZAdmin].[playout] as b")
            strSQL.Append(" 	inner join [10.1.253.88].[EF2KWeb].[dbo].[resda] as c on b.PLAYOUT001=c.resda001 and b.PLAYOUT002=c.resda002  and c.resda021=2")
            strSQL.Append(" ) as b on a.[FSPROGID] = b.[programID] COLLATE Chinese_Taiwan_Stroke_CI_AS  and a.[FNEPISODE] = b.[prgset] ")
            strSQL.Append(" where 1=1 ")


            strSQL.Append(" and a.[FSPROGID] = '" & p_FSPROGID & "'  and [spec] = '" & p_Spec & "' order by [FNEPISODE] asc")

            DropDownList_ProgD.DataSource = objConnectDB.Fun_Get_DataTable(strSQL.ToString, ServerDB.PTSPlayOut, ServerPath.DBptsintranet)
            DropDownList_ProgD.DataValueField = "FNEPISODE"
            DropDownList_ProgD.DataTextField = "pTitle"
            DropDownList_ProgD.DataBind()
            '顯示表單資訊
            Call sub_ShowPlayOutDetail()
        Catch ex As Exception
            Label_ErrorMessage.Text = "產生子集下拉清單 發生錯誤"
        End Try
    End Sub



    Protected Sub GridView_Prog_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView_Prog.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            CType(e.Row.Cells(0).FindControl("LinkButton_ProgNo"), LinkButton).CommandArgument = e.Row.RowIndex
        End If
    End Sub


    Protected Sub DropDownList_ProgD_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownList_ProgD.SelectedIndexChanged
        '顯示表單資訊
        Call sub_ShowPlayOutDetail()
    End Sub
    Private Sub sub_ShowPlayOutDetail()
        '顯示節目分段暨主控播出資訊表
        Try
            Dim objConnectDB As New ConnectDB
            Dim strSQL As New StringBuilder
            Dim str_FSPROGID As String = Label_ProgNo.Text
            Dim str_prgset As String = DropDownList_ProgD.SelectedValue
            Dim str_SPec As String = HFd_Spec.Value
            '串分段明細的Key
            Dim str_playout002 As String = ""
            Dim str_DeptId As String = ""

            Dim wk_TableForm As Data.DataTable
            strSQL.Append("SELECT top 1 [playout001],[playout002],[programID],[programNT],[prgset],[categories],[spec],[length_m],[length_s],[alerts],[alertsName],[customName],[customS],[title],[titleName],[titleS],[programSide],[liveSide],[timeSide],[Replay],[video],[stereo],[user_ID],[logo],[note],[smoking],[ch1],[ch2],[ch3],[ch4],[grade],[resal001],[resak002],[resal003] ")
            strSQL.Append(" FROM [10.1.253.88].[EF2KWeb].[EZAdmin].[playout] ")
            strSQL.Append(" inner join [10.1.253.88].[EF2KWeb].[dbo].[resda] on PLAYOUT001=resda001 and PLAYOUT002=resda002  and resda021=2 ")
            strSQL.Append(" left outer join [10.1.253.88].[EF2KWeb].[dbo].[resak] on user_ID = resak001")
            strSQL.Append(" left outer join [10.1.253.88].[EF2KWeb].[dbo].[resal] on resak015=resal001")

            strSQL.Append(" Where [programID] = '" & str_FSPROGID & "' and [prgset] = '" & str_prgset & "' and spec = '" & str_SPec & "'  Order by [playout002] Desc")
            wk_TableForm = objConnectDB.Fun_Get_DataTable(strSQL.ToString, ServerDB.PTSPlayOut, ServerPath.DBptsintranet)


            'GridView_Form.DataSource = wk_TableForm
            'GridView_Form.DataBind()


            If wk_TableForm.Rows.Count > 0 Then
                Panel_PlayOut.Visible = True
                Label_NoData.Text = ""

                With wk_TableForm.Rows(0)
                    '2011/07/01 顯示表單編號
                    Label_FlowNo.Text = .Item("playout002")

                    str_playout002 = .Item("playout002")
                    '顯示申請人資訊
                    Label_user_Name.Text = .Item("resak002")

                    Select Case Left(.Item("resal001"), 2)
                        Case "01"
                            Label_Channel.Text = "公共電視台"
                        Case "02"
                            Label_Channel.Text = "原住民電視台"
                        Case "03"
                            Label_Channel.Text = "客家電視台"
                        Case Else
                            Label_Channel.Text = "[未知]"
                    End Select
                    Label_Dept.Text = .Item("resal003")
                    '顯示節目名稱
                    Label_programID.Text = .Item("programID")
                    'Label_programName.Text = objConnectDB.Fun_Get_Value("Select [FSWEBNAME] from [PTSProgram].[dbo].[vwTBSProgInfo] where [FSPROGID] = '" & .Item("programID") & "'", ServerDB.PTSPlayOut, ServerPath.DBptsintranet)
                    Label_programName.Text = Label_FSPGMNAME.Text

                    '顯示分級資訊
                    Label_grade.Text = .Item("grade")
                    '顯示集別
                    Label_prgset.Text = .Item("prgset")
                    '顯示分級名稱
                    Label_FSPGDNAME.Text = objConnectDB.Fun_Get_Value("SELECT [FSPGDNAME]  FROM [PTSProgram].[dbo].[TBPROG_D] where [FSPROGID] = '" & .Item("programID") & "' and [FNEPISODE] = " & .Item("prgset"), ServerDB.PTSPlayOut, ServerPath.DBptsintranet)
                    '顯示製作人
                    Label_programNT.Text = .Item("programNT")
                    Label_programNTName.Text = objConnectDB.Fun_Get_Value("Select [resak002] from [10.1.253.88].[EF2KWeb].[dbo].[resak] where resak001 = '" & .Item("programNT") & "'", ServerDB.PTSPlayOut, ServerPath.DBptsintranet)
                    '顯示帶別
                    Select Case .Item("categories")
                        Case "1"
                            Label_categories.Text = "完成帶"
                        Case "2"
                            Label_categories.Text = "播出帶"
                        Case Else
                            Label_categories.Text = "[未知]"
                    End Select
                    '顯示規格
                    Select Case .Item("spec")
                        Case "1"
                            Label_spec.Text = "SD"
                        Case "2"
                            Label_spec.Text = "HD"
                        Case Else
                            Label_spec.Text = "[未知]"
                    End Select
                    '顯示總長
                    Label_length_m.Text = .Item("length_m")
                    Label_length_s.Text = .Item("length_s")
                    '顯示主控插播帶 - 吸菸卡
                    Select Case .Item("smoking")
                        Case "1"
                            Label_smoking.Text = "要 10秒"
                        Case "2"
                            Label_smoking.Text = "不要"
                        Case Else
                            Label_smoking.Text = "[未知]"
                    End Select
                    '顯示主控插播帶 - 警示卡
                    Panel_alerts.Visible = False
                    Panel_custom.Visible = False
                    Select Case .Item("alerts")
                        Case "1"
                            Label_alerts.Text = "要 15秒"
                            Panel_alerts.Visible = True
                            '樣板類別
                            Select Case .Item("alertsName")
                                Case "1", "2", "3", "4", "5", "6"
                                    Label_alertsName.Text = "制式版" & .Item("alertsName")
                                Case "7"
                                    Label_alertsName.Text = "客製版"
                                    Panel_custom.Visible = True
                                    Label_customName.Text = .Item("customName")
                                    Label_customS.Text = .Item("customS")
                            End Select
                        Case "2"
                            Label_alerts.Text = "不要"
                        Case Else
                            Label_alerts.Text = "[未知]"
                    End Select
                    '顯示主控插播帶 - 檔名總片頭
                    Panel_title.Visible = False
                    Select Case .Item("title")
                        Case "1"
                            Label_title.Text = "要"
                            Panel_title.Visible = True
                            Label_titleName.Text = .Item("titleName")
                            Label_titles.Text = .Item("titles")
                        Case "2"
                            Label_title.Text = "不要"
                        Case Else
                            Label_title.Text = "[未知]"
                    End Select
                    '主控鏡面
                    Select Case .Item("programSide")
                        Case "1"
                            Label_programSide.Text = "要"
                        Case "2"
                            Label_programSide.Text = "不要"
                        Case Else
                            Label_programSide.Text = "[未知]"
                    End Select
                    Select Case .Item("liveSide")
                        Case "1"
                            Label_liveSide.Text = "要"
                        Case "2"
                            Label_liveSide.Text = "不要"
                        Case Else
                            Label_liveSide.Text = "[未知]"
                    End Select
                    Select Case .Item("timeSide")
                        Case "1"
                            Label_timeSide.Text = "要"
                        Case "2"
                            Label_timeSide.Text = "不要"
                        Case Else
                            Label_timeSide.Text = "[未知]"
                    End Select
                    Select Case .Item("Replay")
                        Case "1"
                            Label1_Replay.Text = "要"
                        Case "2"
                            Label1_Replay.Text = "不要"
                        Case Else
                            Label1_Replay.Text = "[未知]"
                    End Select
                    Select Case .Item("video")
                        Case "1"
                            Label_video.Text = "要"
                        Case "2"
                            Label_video.Text = "不要"
                        Case Else
                            Label_video.Text = "[未知]"
                    End Select
                    Select Case .Item("stereo")
                        Case "1"
                            Label_stereo.Text = "要"
                        Case "2"
                            Label_stereo.Text = "不要"
                        Case Else
                            Label_stereo.Text = "[未知]"
                    End Select
                    Select Case .Item("logo")
                        Case "1"
                            Label_logo.Text = "右邊 (須上簽核准) "
                        Case Else
                            Label_logo.Text = "-"
                    End Select
                    Label_Ch1.Text = .Item("ch1")
                    Label_Ch2.Text = .Item("ch2")
                    Label_Ch3.Text = .Item("ch3")
                    Label_Ch4.Text = .Item("ch4")
                    '顯示破口段數
                    GridView_TC.DataSource = objConnectDB.Fun_Get_DataTable("SELECT [id_auto],[TCBegin],[TCEnd],[length]  FROM [10.1.253.88].[EF2KWeb].[EZAdmin].[playoutB]  Where [playoutB002] ='" & str_playout002 & "' Order by [id_auto];", ServerDB.PTSPlayOut, ServerPath.DBptsintranet)
                    GridView_TC.DataBind()

                    '顯示備註
                    Literal_Note.Text = .Item("note").Replace(" ", "&nbsp;").ToString.Replace(vbCrLf, "<br />")

                End With
            Else
                Panel_PlayOut.Visible = False
                Label_NoData.Text = "無播出資訊表資訊"
            End If
        Catch ex As Exception
            Label_ErrorMessage.Text = "顯示節目分段暨主控播出資訊表 發生錯誤"
        End Try
    End Sub

    Protected Sub CheckBox_pCheck_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBox_pCheck.CheckedChanged
        Call sub_SearchProg()
    End Sub

    Protected Sub CheckBox_pCheck0_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBox_pCheck0.CheckedChanged
        Call sub_ShowFormDetail(Label_ProgNo.Text, HFd_Spec.Value)
    End Sub
End Class
